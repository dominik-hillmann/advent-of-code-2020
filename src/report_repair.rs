use std::env;
use std::fs::File;
use std::vec::Vec;
use std::path::Path;
use std::io::Read;
use std::io;


fn collect_args() -> Vec<String> {
    let args: Vec<String> = env::args().collect();

    args
}

fn read_input(path_name: String) -> Vec<i32> {
    let path = Path::new(&path_name);
    println!("Is file: {}", path.is_file());
    println!("Is dir: {}", path.is_dir());
    println!("Exists: {}", path.exists());
    println!("Parent: {}", path.parent().unwrap().to_str().unwrap());
    println!("{}", path.display());

    let mut content = String::new();
    let mut file: File = File::open(&path).expect("Could not read file.");
    file.read_to_string(&mut content).expect("Unable to read.");

    let numbers: Vec<i32> = content
        .lines()
        .map(|year_str| year_str.parse::<i32>().unwrap())
        .collect();
    
    numbers
}


fn find_summand_to_2020(mut from: Vec<i32>) -> Result<[i32; 2], io::Error> {
    // smallest first
    from.sort();

    for (i, summand_1) in from.iter().enumerate() {
        println!("{}, {}", i, summand_1);

        for (j, summand_2) in from.iter().enumerate().rev() {
            if j <= i { 
                break;
            }

            if summand_1 + summand_2 == 2020 {
                return Ok([*summand_1, *summand_2]);
            }
        }
    }

    return Err(io::Error::last_os_error());
}


fn find_3_summands_to_2020(mut from: Vec<i32>) -> Result<[i32; 3], io::Error> {
    from.sort();

    for summand_i in from.iter() {
        for summand_j in from.iter() {
            for summand_k in from.iter() {

                let any_equal = summand_i == summand_j ||
                    summand_i == summand_k ||
                    summand_j == summand_k;
                
                if any_equal {
                    continue;
                }

                if summand_i + summand_j + summand_k == 2020 {
                    return Ok([*summand_i, *summand_j, *summand_k]);
                }

            }
        }
    }

    return Err(io::Error::last_os_error());
}

pub fn calc() {
    let mut args: Vec<String> = collect_args();
    // Hard-earned lesson: command line arg indexed 0 is always the path to binary itself!
    let dir_name: String = args.remove(1);

    let numbers = read_input(dir_name);

    let found_2_summands = match find_summand_to_2020(numbers.clone()) {
        Ok(nums) => nums,
        Err(error) => panic!("Did not find fitting numbers: {}", error)
    };

    println!(
        "The numbers are {} and {}, which multiply to {}.", 
        found_2_summands[0],
        found_2_summands[1],
        found_2_summands[0] * found_2_summands[1]
    );

    let found_3_summands = match find_3_summands_to_2020(numbers.clone()) {
        Ok(nums) => nums,
        Err(error) => panic!("Did not find fitting numbers: {}", error)
    };

    println!(
        "The three numbers adding up to 2020 are: {}, {} and {}. They multiply to {}.",
        found_3_summands[0],
        found_3_summands[1],
        found_3_summands[2],
        found_3_summands[0] * found_3_summands[1] * found_3_summands[2]       
    )
}